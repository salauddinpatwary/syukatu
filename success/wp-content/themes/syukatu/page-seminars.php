<?php get_header();?>
<div id="container" class="<?php echo page_name();?>">
<article>
<div id="content">
<h1 class="page_title"><?php the_title();?></h1>
<?php get_template_part('social');?>
<div id="tigai">
<p class="point class01">無料だから騙しがない！<a href="<?php echo home_url();?>/about"></a></p>
<p class="point class02">同じ立場の仲間が増える！<a href="<?php echo home_url();?>/about"></a></p>
<p class="point class03">就活だけじゃない、人生の教養が学べる！<a href="<?php echo home_url();?>/about"></a></p>
<p class="point class04">仕事をする意味、生きる意味を考えませんか？<a href="<?php echo home_url();?>/about"></a></p>
</div>
<h2 class="page_title second_title">セミナー情報一覧</h2>
<div id="top_seminar">
<div id="rss"><a href="<?php echo home_url();?>/category/seminar/feed"></a></div>
<?php $paged = get_query_var('paged');
query_posts('category_name=seminar&paged='.$paged);
if(have_posts()){?>
<dl class="dl_list">
<?php 
while(have_posts()){the_post();
?>
<dt><?php the_time("Y年m月d日"); ?></dt>
<dd><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></dd>
<?php }?>
</dl>
<div class="pagelink"><?php wp_pagenavi();  wp_reset_query();?></div>
<?php }else{?>
<p class="coming buru">coming soon</p>
<?php }wp_reset_query();?>
</div>
</div>
</article>
<?php get_sidebar();?>
</div>
<?php get_footer();?>