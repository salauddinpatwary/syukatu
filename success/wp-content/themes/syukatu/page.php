<?php get_header();?>
<div id="container" class="<?php echo page_name();?>">
<article>
<div id="content">
<h1 class="page_title"><?php the_post();the_title();?></h1>
<?php if(!is_page(array('contact','join','membersetting','memberlogin','privacypolicy'))){get_template_part('social');}?>
<div class="content_post">
<?php the_content();?>
</div>
</div>
</article>
<?php get_sidebar();?>
</div>
<?php get_footer();?>