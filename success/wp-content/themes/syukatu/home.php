<?php get_header();?>
<section>
<div id="top_image">
<div id="image">
<div id="fire01"></div>
<div id="top_koushi"><h1 class="none">西田芳明の勝ち組人勢塾！～就活・学校・恋愛・起業～</h1></div>
<div id="catch01" class="top_catch les"><span>将来の夢を見つけたい！</span></div>
<div id="catch02" class="top_catch les"><span>起業したい！</span></div>
<div id="catch03" class="top_catch les"><span>人間関係を上手くしたい！</span></div>
<div id="catch04" class="top_catch les"><span>面接官を感動させたい！</span></div>
</div>
</div>
</section>
<div id="container">
<article>

<div id="content">
<h2 id="top_h01">他の就活塾とはココが違う！</h2>
<div id="tigai">
<p class="point class01">無料だから騙しがない！<a href="<?php echo home_url();?>/about"></a></p>
<p class="point class02">同じ立場の仲間が増える！<a href="<?php echo home_url();?>/about"></a></p>
<p class="point class03">就活だけじゃない、人生の教養が学べる！<a href="<?php echo home_url();?>/about"></a></p>
<p class="point class04">仕事をする意味、生きる意味を考えませんか？<a href="<?php echo home_url();?>/about"></a></p>
<?php if(!is_user_logged_in()){
?>
<p class="message buru"><span class="none">来たれ同志よ！まずは無料セミナーで体験せよ</span></p>
<?php }?>
</div>
<h3 id="top_h02">無料セミナー情報</h3>
<div id="top_seminar">
<?php $seminar = new WP_query('category_name=seminar&posts_per_page=5');
if($seminar -> have_posts()){?>
<dl class="dl_list">
<?php 
while($seminar -> have_posts()){$seminar -> the_post();
?>
<dt><?php the_time("Y年m月d日"); ?></dt>
<dd><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></dd>
<?php }wp_reset_postdata();?>
</dl>
<p class="txr link_list"><a href="<?php echo home_url();?>/seminars"><img src="<?php bloginfo('template_url'); ?>/img/link-list.png" width="132" height="73" alt="セミナー一覧へ"></a></p>
<?php }else{?>
<p class="coming buru">coming soon</p>
<?php }?>

<div id="rss"><a href="<?php echo home_url();?>/category/seminar/feed"></a></div>
</div>
<?php /*
<h3 id="<?php if(is_user_logged_in()){echo'top_h04';}else{echo'top_h03';}?>">無料セミナー動画</h3>
<?php
//未ログインだと表示
if(!is_user_logged_in()){echo '<p><a href="'.home_url().'/join"><img src="'.get_bloginfo('template_url').'/img/video-member.png" width="736" height="60" alt="無料メンバー登録"></a></p>';
}?>
<div id="top_video">
<?php

//セミナー動画リスト
$video = new WP_query('category_name=video&posts_per_page=9');
if($video -> have_posts()){?>
<ul>
<?php 
$i=0;
while($video -> have_posts()){$video -> the_post();?>
<li<?php
if( $i%3 == 0){echo ' class="clear"';}

?>><p><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php

if(is_user_logged_in()){
//ログイン時
if(post_custom('full')){
$url = "http://gdata.youtube.com/feeds/api/videos/".post_custom('full');
}else{
//fullがない場合はダイジェストを使用
$url = "http://gdata.youtube.com/feeds/api/videos/".post_custom('digest');
}
}else{
//未ログイン
echo '<span class="digest"></span>';

$url = "http://gdata.youtube.com/feeds/api/videos/".post_custom('digest');
}
$list = file_get_contents($url);
//var_dump($list);
$rsp = simplexml_load_file($url);
$title = $rsp->children('http://search.yahoo.com/mrss/')->group->title;
$thumb = $rsp->children('http://search.yahoo.com/mrss/')->group->thumbnail[0]->attributes()->url;
echo '<img src="'.$thumb.'" alt=""><br>'.get_the_title();
?></a></p><p class="txr time"><?php the_time("Y年m月d日"); 

?></p></li>
<?php

$i++;
}?>
</ul>
<p class="txr link_list clear"><a href="<?php echo home_url();?>/video"><img src="<?php bloginfo('template_url'); ?>/img/link-list.png" width="132" height="73" alt="動画一覧へ"></a></p>
<?php }else{?>
<p class="coming buru">coming soon</p>
<?php }?>
</div>*/?>
</div>
</article>
<?php get_sidebar();?>
<div class="clear"></div>
</div>
<?php get_footer();?>