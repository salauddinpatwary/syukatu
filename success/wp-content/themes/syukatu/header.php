﻿<!DOCTYPE HTML>
<html <?php language_attributes(); ?> xmlns:fb='http://www.facebook.com/2008/fbml'>
<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<title><?php bloginfo('name');?></title>
<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" media="all" />
<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url'); ?>/favicon.ico" />
<link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/screenshot.png" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<?php
wp_deregister_script('jquery');
wp_enqueue_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js', array(), '1.7.1');
wp_head();
?>
</head>
<body <?php body_class(); ?>>
<header>
<div id="header">
<hgroup>
<h1><a href="<?php echo home_url();?>/"><img src="<?php bloginfo('template_url'); ?>/img/syukatu-logo.png" width="265" height="112" alt="<?php bloginfo('name');?>"></a></h1>
<h2><?php bloginfo('description');?></h2>
</hgroup>
<div id="login">
<ul>
<?php if(is_user_logged_in()){
//未ログイン
?>
<li id="hed_logout"><a class="les" href="<?php echo home_url();?>/?a=logout"><span>ログアウト</span></a></li>
<li id="hed_setting"><a class="les" href="<?php echo home_url();?>/membersetting"><span>情報設定</span></a></li>
<?php }else{
//ログイン中
?>
<li id="head_login"><a class="les" href="<?php echo home_url();?>/memberlogin"><span>ログイン</span></a></li>
<li id="hed_join"><a class="les" href="<?php echo home_url();?>/join"><span>無料登録</span></a></li>
<?php }?>
</ul>
</div>
</div>
</header>
<div id="global_nav">
<nav>
<ul>
<li><a href="<?php echo home_url();?>/about">就活志塾とは？</a></li>
<li><a href="<?php echo home_url();?>/seminars">セミナー情報</a></li>
<li><a href="<?php echo home_url();?>/media">メディア掲載</a></li>
<li><a href="<?php echo home_url();?>/voice">参加者の声</a></li>
<li><a href="<?php echo home_url();?>/kaban">社長のカバン持ち体験</a></li>
<li><a href="<?php echo home_url();?>/contact">お問い合わせ</a></li>
</ul>
</nav>
</div>