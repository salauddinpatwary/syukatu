<?php get_header();?>
<link href="style.css" rel="stylesheet" type="text/css" />
<article>
<div id="aboutcontents">
<script src="<?php the_post();bloginfo('template_url'); ?>/js/about.js"></script>
<div id="about_t">
<ul>
<li id="about01"><span></span>就職活動だけじゃない</li>
<li id="about03"><span></span>モチベーションをアップしたい</li>
<li id="about04"><span></span>今の悩みを解決したい</li>
<li id="about02"><span></span>人生で一番大切なこととは？</li>
</ul>
</div>
<div id="about_c">
<ul>
<li id="about05"><a href="#about09"></a></li>
<li id="about10"><a href="#about09"></a></li>
<li id="about06"><a href="#about09"></a></li>
<li id="about07"><a href="#about09"></a></li>
</ul>
</div>
<div id="about_r">
<p id="about11"><a href="#about09"></a></p>
<p id="about08"><a href="#about09"><img src="<?php bloginfo('template_url'); ?>/img/about_voice.png" width="231" height="133" alt="参加した学生の声" /></a></p>
<p id="about09"><a href="#about09"></a></p>
</div>
<div id="about_01"></div>
<div id="about_02"></div>
<div id="about_03"></div>
<div id="about_04"></div>
</div>
<div id="container" class="<?php echo page_name();?>">
<div id="content">
<?php get_template_part('social');?>
<noscript><p class="txc req">当サイトはjavascriptを無効にしていると正しくコンテンツが表示されませんので、ぜひ有効にして下さい。</p></noscript>
<div class="content_post">
<div id="reason"><?php 
$gid = idget('/about/reason');
$gpost = get_post($gid);
echo '<h1 class="page_title">'.$gpost->post_title.'</h1>';
echo wpautop($gpost->post_content);?></div>
<div id="lesson" style="display:none"><?php 
$gid = idget('/about/lesson');
$gpost = get_post($gid);
echo '<h1 class="page_title">'.$gpost->post_title.'</h1>';
echo wpautop($gpost->post_content);?></div>
<div id="teacher" style="display:none"><?php 
$gid = idget('/about/teacher');
$gpost = get_post($gid);
echo '<h1 class="page_title">'.$gpost->post_title.'</h1>';
echo wpautop($gpost->post_content);?></div>
<div id="voice" style="display:none"><?php 
$gid = idget('/about/voice');
$gpost = get_post($gid);
echo '<h1 class="page_title">'.$gpost->post_title.'</h1>';
echo wpautop($gpost->post_content);?></div>
<div id="communicate" style="display:none"><?php 
$gid = idget('/about/communicate');
$gpost = get_post($gid);
echo '<h1 class="page_title">'.$gpost->post_title.'</h1>';
wp_reset_query();wp_reset_postdata();
$com_page = new Wp_query('post_type=page&p=191');
while($com_page ->have_posts()){
$com_page -> the_post();
the_content();
}wp_reset_postdata();
//echo wpautop($gpost->post_content);
wp_reset_postdata();
?></div>
<div id="nakama" style="display:none"><?php 
$gid = idget('/about/nakama');
$gpost = get_post($gid);
echo '<h1 class="page_title">'.$gpost->post_title.'</h1>';
echo wpautop($gpost->post_content);?></div>
<div id="advantage" style="display:none"><?php 
$gid = idget('/about/advantage');
$gpost = get_post($gid);
echo '<h1 class="page_title">'.$gpost->post_title.'</h1>';
echo wpautop($gpost->post_content);?></div>
</div>
</div>
<?php get_sidebar();?>
</div>
</article>
<?php get_footer();?>