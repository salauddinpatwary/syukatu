<?php get_header();?>
<div id="container" class="<?php echo page_name();?>">
<article>
<div id="content">
<h1 class="page_title"><?php the_post();the_title();?></h1>
<?php get_template_part('social');?>
<div class="content_post">
<p><strong>満足度100％！<br>
</strong>参加者の生の声を載せております。</p>
</div>
<?php $paged = get_query_var('paged');  
query_posts('post_type=voices&posts_per_page=28&paged='.$paged)
?><p class="small">＜クリックで拡大します。＞</p>
<ul id="voice_list">
<?php while(have_posts()){the_post();?>
<li>
<p><a href="<?php $image_id = get_post_thumbnail_id(); $image_url = wp_get_attachment_image_src($image_id, true);
echo $image_url[0]; ?>" class="fancyimg" rel="voice"><?php if(has_post_thumbnail()){the_post_thumbnail('thumbnail');}?></a></p>
<p class="voice_file"><?php the_title();?></p>
</li>
<?php }?>
</ul>
<div class="pagelink"><?php wp_pagenavi();  wp_reset_query();?></div>
<h3 class="page_title">セミナーギャラリー</h3>
<div class="content_post">
<?php the_content();?>
</div>
</div>
</article>
<?php get_sidebar();?>
</div>
<?php get_footer();?>