<footer><?php wp_reset_query();wp_reset_postdata();?>
<div id="footer"><div id="footer_bottom">
<div id="footer_in">
<p id="pagetop"><a href="#"></a></p>
<ul>
<li><a href="<?php echo home_url();?>/">トップページ</a></li>
<li><a href="<?php echo home_url();?>/partnar">協賛学校・団体</a></li>
<li><a href="<?php echo home_url();?>/privacypolicy">個人情報保護方針</a></li>
<li><a href="<?php echo home_url();?>/company">運営会社</a></li>
<li><a href="<?php echo home_url();?>/contact">お問い合わせ</a></li>
</ul>
<p id="copy">Produced by <a href="http://e-shinwa.net/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/shinwa-hd-logo.png" width="130" height="24" alt="進和ホールディングス"></a><br>
Copyright(c) 2013 <a href="http://e-shinwa.net/" target="_blank">Shinwa HD Corporation.</a> All Rights Reserved.</p>
</div>
</div></div>
</footer>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-40367713-1', 'syukatu.info');
  ga('send', 'pageview');

</script>
<script src="<?php bloginfo('template_url'); ?>/js/scriptset.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.jrumble.min.js"></script>
<script>
$('#kaban_fire').css({opacity:0,width:0});
$(window).scroll(function(){
$('#kaban_fire').animate({opacity:1,width:'216px',left:'-47px'},{duraiton:3000});

});
$(function(){
$("a.fancyimg").fancybox({'titleShow':false});
$("a.iframe").fancybox({
'width':480,
'height':360,
'cyclic':true,
'showNavArrows':true
});
<?php if(is_home()){?>
setTimeout(function(){$('#catch01').fadeIn(2000);},2000);
setTimeout(function(){$('#catch02').fadeIn(2000);},4000);
setTimeout(function(){$('#catch03').fadeIn(2000);},6000);
setTimeout(function(){$('#catch04').fadeIn(2000);},8000);
<?php }

if ( function_exists('wp_is_mobile') && !wp_is_mobile()){?>
$('.link_list a,#header h1,.buru,.post-categories li a,#about_c ul li').jrumble({
rangeX: 5,
rangeY: 5,
rangeRot: 4,
rumbleSpeed: 10,
rumbleEvent: 'hover'
});
$('#tigai .point a,#about_t ul li span').jrumble({
rangeX: 2,
rangeY: 2,
rangeRot: 4,
rumbleSpeed: 10,
rumbleEvent: 'constant'
});
anime();
function anime(){
$('#fire01').animate({opacity:0.2,left:'-15px'},{duration:600,easing:'jswing'}).animate({opacity:0.7,left:'-20px'},{duration:600,easing:'jswing'});
setTimeout(anime(),1800)
}
<?php }//スマホで未動作　完?>
});
</script>
<?php wp_footer();?>
</body>
</html>