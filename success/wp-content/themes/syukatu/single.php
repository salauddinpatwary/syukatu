<?php get_header();?>
<div id="container">
<article>
<div id="content">
<?php if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('<p id="pankuzu">','</p>'); } ?>
<h1 class="single_title"><?php the_post();the_title();?></h1>
<time datetime="<?php the_time("Y-m-d"); ?>" pubdate="pubdate"><?php the_time("Y年m月d日"); ?></time>
<?php the_category();?>
<?php get_template_part('social');?>
<div class="content_post">
<?php

//セミナー動画
if(in_category('video')){
if(is_user_logged_in()){
$video_id = post_custom('full');
if(post_custom('full')){
	//$url = "http://gdata.youtube.com/feeds/api/videos/".post_custom('full');
	echo '<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/'.post_custom('full').'?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>';
}else{
//fullがない場合はダイジェストを使用
	//$url = "http://gdata.youtube.com/feeds/api/videos/".post_custom('digest');
	echo '<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/'.post_custom('digest').'?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>';
}
}else{
//非メンバー
echo '<p><a href="'.home_url().'/join"><img src="'.get_bloginfo('template_url').'/img/video-member.png" width="736" height="60" alt="無料メンバー登録"></a></p>
<p class="txc"><a href="'.home_url().'/memberlogin">ログインはこちら</a></p>';
echo '<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/'.post_custom('digest').'?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>';
//$video_id = post_custom('digest');
//未ログイン
	//$url = "http://gdata.youtube.com/feeds/api/videos/".post_custom('digest');
}
	//$rsp = simplexml_load_file($url);
	//$title = $rsp->children('http://search.yahoo.com/mrss/')->group->title;
	//$des = $rsp->children('http://search.yahoo.com/mrss/')->group->description;
/*?>
<div id="screen"><?php if(!is_user_logged_in()){echo '<div class="digest"></div>';}?>
<iframe width="640" height="480" src="http://www.youtube.com/embed/<?php echo $video_id;?>?rel=0&egm=1&wmode=transparent" frameborder="0" allowfullscreen></iframe>
<p><strong><?php echo $title;?></strong></p>
</div>
<?php */

//ここまでセミナー動画ページ
}
the_content();
if(in_category('seminar')){

//セミナーページの予約
if(post_custom('seminar')=='true'){
get_template_part('seminar-form');}
elseif(post_custom('seminar')=='full'){
echo '<p class="txc buru red">申し訳ございません。満席となってしまいました。</p>';
}
}

//参加者の声
if( 'voices' == get_post_type() ){
the_post_thumbnail('full');
}
?>
</div>
<div id="post_bottom clear">
<?php get_template_part('social_b');?>
<div id="page_link">
<span class="previous buru">
<?php previous_post_link( '%link', '&lt; 前の記事へ',true); ?>
</span>
<span class="next buru">
<?php next_post_link( '%link', '次の記事へ &gt;',true); ?>
</span>
</div><?php if(!in_category('seminar')){comments_template();}?>
</div>
</div>
</article>
<?php get_sidebar();?>
</div>
<?php get_footer();?>