<?php 
/*
Template Name: Archive
*/
get_header();?>
<div id="container" class="<?php echo page_name();?>">
<article>
<div id="content">
<h1 class="page_title"><?php
if(is_page()){the_title();}else{
if(is_category()){single_cat_title();}
elseif ( is_day() ){echo get_the_date();}
elseif ( is_month() ) {echo get_the_date('Y年F');}
elseif ( is_year() ){echo get_the_date('Y年');}
elseif(is_tag()){single_tag_title();}
elseif(is_post_type_archive()){post_type_archive_title();}
elseif(is_author){the_author();echo 'の';}
elseif(is_tax() ){single_term_title();}
else {echo ('新着');}
echo '一覧';}?></h1>
<?php

get_template_part('social');

//どのアーカイブか？
$paged = get_query_var('paged');
if(is_page('kaban_list')){
query_posts('post_type=kabanblog&paged='.$paged);
}
elseif(is_page('media')){
query_posts('category_name=media&paged='.$paged);
}
elseif(is_page('video')){
query_posts('category_name=video&paged='.$paged);
}
if(have_posts()){
?>
<ul class="archive_list">
<?php 
while(have_posts()){the_post();
?>
<li>
<?php if(has_post_thumbnail()){?>
<div class="archive_img">
<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('thumbnail');?></a>
</div>
<?php }elseif(in_category('video')){ ?>
<div class="archive_img">
<?php /*<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
<?php 

if(is_user_logged_in()){
//ログイン時
if(post_custom('full')){
	$url = "http://gdata.youtube.com/feeds/api/videos/".post_custom('full');
}else{
//fullがない場合はダイジェストを使用
	$url = "http://gdata.youtube.com/feeds/api/videos/".post_custom('digest');}
}else{
//未ログイン
	$url = "http://gdata.youtube.com/feeds/api/videos/".post_custom('digest');
}

	$rsp = simplexml_load_file($url);
	$title = $rsp->children('http://search.yahoo.com/mrss/')->group->title;
	$thumb = $rsp->children('http://search.yahoo.com/mrss/')->group->thumbnail[0]->attributes()->url;
	echo '<img src="'.$thumb.'" alt="'.$title.'">';
?></a>*/?>
</div>
<?php }?>
<div class="archive_right<?php

//サムネイルがない、セミナー動画出ない場合はひろく
if(!has_post_thumbnail()&&!in_category('video')){echo ' noimg';}

?>"><h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
<p class="time">投稿日：<?php the_time("Y年m月d日"); ?></p>
<div class="excerpt clear"><?php the_excerpt();?></div>
</div>
</li>
<?php }?>
</ul>
<div class="pagelink"><?php wp_pagenavi();  wp_reset_query();?></div>
<?php }else{?>
<p class="coming buru">coming soon</p>
<?php }?>
</div>
</article>
<?php get_sidebar();?>
</div>
<?php get_footer();?>