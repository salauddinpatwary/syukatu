<?php get_header();?>
<div id="container">
<article>
<div id="content">
<h1 class="single_title"><?php the_title();?></h1>
<time datetime="<?php the_post();the_time("Y-m-d"); ?>" pubdate="pubdate">投稿日：<?php the_time("Y年m月d日"); ?></time>
<?php get_template_part('social');?>
<div class="content_post">
<div class="flor kaban_img">
<?php if(has_post_thumbnail()):?>
<?php the_post_thumbnail('thumbnail');?>
<?php else: ?>
<img src="<?php bloginfo('template_url'); ?>/img/kaban_img.png" width="150" height="150" alt="<?php the_title(); ?>" /><?php endif; ?>
</div>
<?php the_content();?>
</div>
<div id="post_bottom clear">
<?php get_template_part('social_b');?>
<div id="page_link">
<span class="previous">
<?php previous_post_link( '%link', '&lt; 前の記事へ'); ?>
</span>
<span class="next">
<?php next_post_link( '%link', '次の記事へ &gt;'); ?>
</span>
</div><?php comments_template();?>
</div>
</div>
</article>
<?php get_sidebar();?>
</div>
<?php get_footer();?>